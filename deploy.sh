echo 'EurekaServer releaser'
echo 'pulling latest revision ...'
git pull
echo 'latest revision pulled!'

echo 'deploying EurekaServer'
docker stop eurekaserver
docker rm eurekaserver
docker build -t eurekaserver .
docker run -d -p 8761:8761 --name eurekaserver eurekaserver
