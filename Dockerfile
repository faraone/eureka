FROM maven:3.8.1-openjdk-16-slim AS build
COPY src /home/eurekaserver/src
COPY pom.xml /home/eurekaserver
RUN mvn -f /home/eurekaserver/pom.xml clean package

FROM amazoncorretto:16
COPY --from=build /home/eurekaserver/target/*.jar /usr/local/lib/eureka-server.jar
EXPOSE 8761
ENTRYPOINT ["java","-jar","/usr/local/lib/eureka-server.jar"]